/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author exhau
 */
public class Frame1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        //frame.setSize(new Dimension(500,300));
        frame.setSize(500, 300);

        JLabel lblHelloWorld = new JLabel("Hello World!", JLabel.CENTER);
        lblHelloWorld.setBackground(Color.ORANGE);
        lblHelloWorld.setFont(new Font("Verdana", Font.PLAIN, 25));
        lblHelloWorld.setOpaque(true);
        frame.add(lblHelloWorld);

        frame.setVisible(true);
    }
}
